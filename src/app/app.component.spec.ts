import { TestBed, async, ComponentFixture } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { Component, NO_ERRORS_SCHEMA } from '@angular/core';
import { By } from '@angular/platform-browser';
import { HeaderComponent } from './@features/header/header.component';

@Component({
  selector: 'app-basket',
  template: ''
})
class BasketComponent {}


describe('AppComponent', () => {
  let fixture: ComponentFixture<AppComponent>;
  let app: AppComponent;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        HeaderComponent, // This is the actual  HeaderComponent
        BasketComponent // This is a stub of the BasketComponent
      ],
      schemas: [
        NO_ERRORS_SCHEMA
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(AppComponent);
    app = fixture.debugElement.componentInstance;
    fixture.detectChanges();
  }));
  it('should create the app', async(() => {

    expect(app).toBeTruthy();
  }));

  it(`should have as title 'Testing with Angular and Wallaby'`, async(() => {
    expect(app.title).toEqual('Testing with Angular and Wallaby');
  }));

  it('should have a h1 tag with the title', async(() => {
    const titleTag = fixture.debugElement.query(By.css('h1'));

    expect(titleTag.nativeElement.textContent).toEqual('Testing with Angular and Wallaby');

    app.title = 'This is the new title';

    fixture.detectChanges();

    expect(titleTag.nativeElement.textContent).toEqual('This is the new title');
  }));

  /**
   * This test should probably not be in here since we
   * are actually testing the functionality of the header
   */
  it('should have a header saying "Testing is awesome!"', () => {
    const headerSpan = fixture.nativeElement.querySelector('header span');

    expect(headerSpan.textContent).toEqual('Testing is awesome!');
  });

});
