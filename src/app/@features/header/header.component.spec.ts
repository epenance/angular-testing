import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HeaderComponent } from './header.component';
import { Component } from '@angular/core';

describe('HeaderComponent', () => {
  let component: HeaderComponent;
  let fixture: ComponentFixture<HeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HeaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});


/**
 * Testing inputs by using a host component
 */
@Component({
  template: `<app-header [title]="headerTitle"></app-header>`
})
class TestHostComponent {
  headerTitle = 'This is a title in the header';
}

describe('HeaderComponent with HostComponent', () => {
  let component: TestHostComponent;
  let fixture: ComponentFixture<TestHostComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        TestHostComponent, HeaderComponent
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestHostComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should show the header that is supplied by its host', () => {
    const headerTitle = fixture.nativeElement.querySelector('header span');

    expect(headerTitle.textContent).toEqual(component.headerTitle, 'expected the header to show the title provided by the host');

    const newTitle = 'This is a new title';

    component.headerTitle = newTitle;
    fixture.detectChanges();

    expect(headerTitle.textContent).toEqual(newTitle, 'expected the title to be the new title');
  });
});
