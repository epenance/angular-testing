import { async, ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';

import { BasketComponent } from './basket.component';
import { BasketService } from '../basket.service';
import { asyncData } from '../../../../testings/helpers';
import { BasketProductComponent } from '../basket-product/basket-product.component';
import { By } from '@angular/platform-browser';

let basketServiceSpy: { getBasket: jasmine.Spy };

describe('BasketComponent', () => {
  let component: BasketComponent;
  let fixture: ComponentFixture<BasketComponent>;
  const basketData = {
    products: [
      {
        'id': 1,
        'name': 'Fancy pants',
        'price': 300
      },
      {
        'id': 1,
        'name': 'Yolo pants',
        'price': 5
      },
      {
        'id': 1,
        'name': 'Swag pants',
        'price': 400
      }
    ]
  };


  beforeEach(async(() => {
    basketServiceSpy = jasmine.createSpyObj('BasketService', ['getBasket']);
    basketServiceSpy.getBasket.and.returnValue(asyncData(basketData));
    TestBed.configureTestingModule({
      declarations: [
        BasketComponent,
        BasketProductComponent
      ],
      providers: [
        {
          provide: BasketService, useValue: basketServiceSpy
        }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BasketComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should show a line for each product', fakeAsync(() => {
    component.ngOnInit();
    fixture.detectChanges();

    const basketElement: HTMLElement = fixture.nativeElement;
    let products = basketElement.querySelectorAll('li');

    // We should show 0 product lines before the service call resolves
    expect(products.length).toBe(0, 'Expected 0 product lines before the service call resolves');

    tick();
    fixture.detectChanges();

    // Once it resolves we expect it to have 3
    products = basketElement.querySelectorAll('li');

    expect(products.length).toBe(3, 'Expected 3 products lines to be rendered from the data provided');
  }));

  it('should set the selected product on click', fakeAsync(() => {
    component.ngOnInit();
    fixture.detectChanges();
    tick();
    fixture.detectChanges();

    const basketElement: HTMLElement = fixture.nativeElement;
    const products = basketElement.querySelectorAll('li');

    expect(component.selectedProduct).toEqual(undefined, 'Expected the selected product to be undefined until a product has been clicked');

    products[0].click();

    fixture.detectChanges();

    expect(component.selectedProduct).toEqual(basketData.products[0], 'Expected the selectedProduct to be the same as the clicked product');
  }));

  it('should set the active class on the selected product and to be bold', fakeAsync(() => {
    component.ngOnInit();
    fixture.detectChanges();
    tick();
    fixture.detectChanges();

    const basketElement: HTMLElement = fixture.nativeElement;
    const products = basketElement.querySelectorAll('li');

    expect(products[0].classList).not.toContain('active');

    products[0].click();
    fixture.detectChanges();

    expect(products[0].classList).toContain('active');

    const styles = window.getComputedStyle(products[0]);
    const fontWeight = styles.getPropertyValue('font-weight');

    expect(fontWeight).toBe('700');
  }));

  it('should change the selected product to be the product that was selected in app-basket-product', fakeAsync(() => {
    component.ngOnInit();
    fixture.detectChanges();
    tick();
    fixture.detectChanges();


    const products = fixture.debugElement.queryAll(By.css('app-basket-product span'));

    expect(component.selectedProduct).toBe(undefined, 'Expected the selected product to be undefined until we have clicked a product');

    products[1].nativeElement.click();

    fixture.detectChanges();

    expect(component.selectedProduct).toEqual(basketData.products[1], 'Expected the selected product to be equal the product we clicked');

  }));
});
