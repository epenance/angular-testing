import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { BasketService } from '../basket.service';
import { map } from 'rxjs/operators';
import { Basket, Product } from '../basket.interface';

@Component({
  selector: 'app-basket',
  templateUrl: './basket.component.html',
  styleUrls: ['./basket.component.scss']
})
export class BasketComponent implements OnInit {
  products$: Observable<Product[]>;
  selectedProduct: Product;

  constructor(private basketService: BasketService) { }

  ngOnInit() {
    this.products$ = this.basketService.getBasket().pipe(
      map((basket: Basket) => {
        return basket.products;
      })
    );
  }

  setSelectedProduct(product) {
    this.selectedProduct = product;
  }
}
