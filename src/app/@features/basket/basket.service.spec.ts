import { TestBed, inject } from '@angular/core/testing';

import { BasketService } from './basket.service';
import { asyncData, asyncError } from '../../../testings/helpers';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';

let httpClientSpy: { get: jasmine.Spy };

describe('BasketService', () => {
  beforeEach(() => {
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get']);

    TestBed.configureTestingModule({
      providers: [
        BasketService,
        {
          provide: HttpClient, useValue: httpClientSpy
        }
      ]
    });
  });

  it('should be created', inject([BasketService], (service: BasketService) => {
    expect(service).toBeTruthy();
  }));

  it('should fetch the basket by http', inject([BasketService], (service: BasketService) => {
    const expectedBasket = {
      products: [
        {
          'id': 1,
          'name': 'Fancy pants',
          'price': 300
        },
        {
          'id': 1,
          'name': 'Yolo pants',
          'price': 5
        },
        {
          'id': 1,
          'name': 'Swag pants',
          'price': 400
        },
      ]
    };

    httpClientSpy.get.and.returnValue(asyncData(expectedBasket));

    service.getBasket().subscribe(
      basket => expect(basket).toEqual(expectedBasket, 'expected basket'),
      fail
    );

    // It should only have called the service once
    expect(httpClientSpy.get.calls.count()).toBe(1);
  }));


  it('should return an error if the server returns 404', inject([BasketService], (service: BasketService) => {
    const errorResponse = new HttpErrorResponse({
      error: 'A fancy 404 error',
      status: 404,
      statusText: 'Not found'
    });

    httpClientSpy.get.and.returnValue(asyncError(errorResponse));

    service.getBasket().subscribe(
      basket => fail('expected an error, not the basket'),
      error => {
        expect(error.error).toBe('A fancy 404 error');
        expect(error.message).toContain('404');
        expect(error.message).toContain('Not found');
      }
    );
  }));
});
