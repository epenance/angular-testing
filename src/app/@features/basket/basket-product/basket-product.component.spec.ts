import { BasketProductComponent } from './basket-product.component';
import { Product } from '../basket.interface';

describe('BasketProductComponent', () => {
  let component: BasketProductComponent;

  beforeEach(() => {
    component = new BasketProductComponent();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should emit an event with the selected product when a product is selected', () => {
    const product: Product = {
      id: 42,
      name: 'Yolo shirt',
      price: 300
    };

    component.product = product;

    component.selected.subscribe(selectedProduct => expect(selectedProduct).toEqual(product));

    component.handleClick();
  });
});
