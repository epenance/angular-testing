import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Product } from '../basket.interface';

@Component({
  selector: 'app-basket-product',
  templateUrl: './basket-product.component.html',
  styleUrls: ['./basket-product.component.scss']
})
export class BasketProductComponent implements OnInit {
  @Input() product: Product;
  @Output() selected = new EventEmitter<Product>();

  constructor() { }

  ngOnInit() {
  }

  handleClick() {
    this.selected.emit(this.product);
  }

}
