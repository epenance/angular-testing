import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable()
export class BasketService {

  constructor(private http: HttpClient) { }

  getBasket() {
    return this.http.get( environment.apiRoot + '/basket');
  }

}
