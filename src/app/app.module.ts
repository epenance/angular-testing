import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { BasketComponent } from './@features/basket/basket/basket.component';
import { BasketService } from './@features/basket/basket.service';
import { HttpClientModule } from '@angular/common/http';
import { BasketProductComponent } from './@features/basket/basket-product/basket-product.component';
import { HeaderComponent } from './@features/header/header.component';


@NgModule({
  declarations: [
    AppComponent,
    BasketComponent,
    BasketProductComponent,
    HeaderComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule
  ],
  providers: [
    BasketService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
